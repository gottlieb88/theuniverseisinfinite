<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="&#x421;&#x438;&#x43c;&#x443;&#x43b;&#x44f;&#x446;&#x438;&#x44f; &#x412;&#x441;&#x435;&#x43b;&#x435;&#x43d;&#x43d;&#x43e;&#x439;" FOLDED="false" ID="ID_638433748" CREATED="1542434091826" MODIFIED="1542434904445" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="19" RULE="ON_BRANCH_CREATION"/>
<node TEXT="&#x421;&#x43e;&#x43b;&#x43d;&#x435;&#x447;&#x43d;&#x430;&#x44f; &#x421;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x430;" POSITION="right" ID="ID_989909198" CREATED="1542434105030" MODIFIED="1542434626125" HGAP_QUANTITY="52.99999883770946 pt" VSHIFT_QUANTITY="26.999999195337324 pt">
<edge COLOR="#ff00ff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="&#x412;&#x44b;&#x44f;&#x43d;&#x441;&#x438;&#x442;&#x44c; &#x437;&#x430;&#x43a;&#x43e;&#x43d;&#x43e;&#x43c;&#x435;&#x440;&#x43d;&#x43e;&#x441;&#x442;&#x438; &#x434;&#x432;&#x438;&#x436;&#x435;&#x43d;&#x438;&#x44f; &#x43f;&#x43e; &#x44d;&#x43b;&#x43b;&#x438;&#x43f;&#x441;&#x430;&#x43c;" ID="ID_467604517" CREATED="1542434207602" MODIFIED="1542434293671"/>
<node TEXT="&#x414;&#x432;&#x438;&#x436;&#x435;&#x43d;&#x438;&#x435; &#x441;&#x43f;&#x443;&#x442;&#x43d;&#x438;&#x43a;&#x43e;&#x432; &#x432;&#x43e;&#x43a;&#x440;&#x443;&#x433; &#x43f;&#x43b;&#x430;&#x43d;&#x435;&#x442;" ID="ID_674924822" CREATED="1542434260391" MODIFIED="1542434276709"/>
<node TEXT="&#x414;&#x432;&#x438;&#x436;&#x435;&#x43d;&#x438;&#x435; &#x43f;&#x43b;&#x430;&#x43d;&#x435;&#x442; &#x432;&#x43e;&#x43a;&#x440;&#x443;&#x433; &#x441;&#x43e;&#x43b;&#x43d;&#x446;&#x430;" ID="ID_1521980901" CREATED="1542434225613" MODIFIED="1542434258117"/>
</node>
<node TEXT="&#x41e;&#x441;&#x442;&#x430;&#x43b;&#x44c;&#x43d;&#x44b;&#x435; &#x433;&#x430;&#x43b;&#x430;&#x43a;&#x442;&#x438;&#x43a;&#x438;" POSITION="right" ID="ID_1272132242" CREATED="1542434113080" MODIFIED="1542434778533">
<edge COLOR="#0000ff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x442;&#x440;&#x43e;&#x435;&#x43d;&#x438;&#x435; &#x433;&#x430;&#x43b;&#x430;&#x43a;&#x442;&#x438;&#x43a;, &#x43f;&#x43e;&#x445;&#x43e;&#x436;&#x438;&#x445; &#x43d;&#x430; &#x421;&#x43e;&#x43b;&#x43d;&#x435;&#x447;&#x43d;&#x443;&#x44e; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x443;" ID="ID_1478893905" CREATED="1542434748333" MODIFIED="1542434825239"/>
<node TEXT="&#x41f;&#x43e;&#x441;&#x442;&#x440;&#x43e;&#x435;&#x43d;&#x438;&#x435; &#x433;&#x430;&#x43b;&#x430;&#x43a;&#x442;&#x438;&#x43a;, &#x441;&#x438;&#x43b;&#x44c;&#x43d;&#x43e; &#x43e;&#x442;&#x43b;&#x438;&#x447;&#x430;&#x44e;&#x449;&#x438;&#x445;&#x441;&#x44f; &#x43e;&#x442; &#x421;&#x43e;&#x43b;&#x435;&#x447;&#x43d;&#x43e;&#x439; &#x441;&#x438;&#x441;&#x442;&#x435;&#x43c;&#x44b;(&#x43d;&#x430;&#x43f;&#x440;&#x438;&#x43c;&#x435;&#x440;-&#x441; &#x434;&#x432;&#x443;&#x43c;&#x44f; &#x437;&#x432;&#x435;&#x437;&#x434;&#x430;&#x43c;&#x438;)" ID="ID_1865547637" CREATED="1542434774694" MODIFIED="1542434805779"/>
</node>
<node TEXT="&#x421;&#x43c;&#x435;&#x43d;&#x430; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x438;(&#x43f;&#x435;&#x440;&#x435;&#x445;&#x43e;&#x434; &#x432; &#x431;&#x443;&#x434;&#x443;&#x449;&#x435;&#x435; &#x438; &#x43f;&#x440;&#x43e;&#x448;&#x43b;&#x43e;&#x435;)" FOLDED="true" POSITION="left" ID="ID_1740273889" CREATED="1542434307913" MODIFIED="1542434621454" HGAP_QUANTITY="-178.74999425560253 pt" VSHIFT_QUANTITY="70.49999789893633 pt">
<edge COLOR="#00ffff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="&#x421;&#x43c;&#x435;&#x43d;&#x430; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x438;(&#x43f;&#x435;&#x440;&#x435;&#x445;&#x43e;&#x434; &#x432; &#x431;&#x443;&#x434;&#x443;&#x449;&#x435;&#x435; &#x438; &#x43f;&#x440;&#x43e;&#x448;&#x43b;&#x43e;&#x435;)" ID="ID_285642012" CREATED="1542434314902" MODIFIED="1542434357881" HGAP_QUANTITY="-217.74999309331199 pt" VSHIFT_QUANTITY="95.99999713897714 pt"/>
</node>
<node TEXT="&#x41a;&#x430;&#x440;&#x442;&#x438;&#x43d;&#x430; &#x43a;&#x43e;&#x441;&#x43c;&#x43e;&#x441;&#x430; &#x434;&#x43b;&#x44f; &#x43d;&#x430;&#x431;&#x43b;&#x44e;&#x434;&#x430;&#x442;&#x435;&#x43b;&#x44f; &#x441; &#x417;&#x435;&#x43c;&#x43b;&#x438;" FOLDED="true" POSITION="left" ID="ID_27551277" CREATED="1542434831853" MODIFIED="1542434904444" HGAP_QUANTITY="49.249998949468164 pt" VSHIFT_QUANTITY="-111.74999666959059 pt">
<edge COLOR="#007c00"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="" ID="ID_1306900555" CREATED="1542434862174" MODIFIED="1542434885659"/>
</node>
</node>
</map>
